package by.fortegroup.GolubovMax;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import java.net.URL;

public class A extends AppCompatActivity implements View.OnClickListener {

    final private static String[] URLS = new String[]{
            "http://images5.fanpop.com/image/photos/27900000/Ocean-Animals-animals-27960311-1920-1200.jpg",
            "http://www.heartofgreen.org/.a/6a00d83451cedf69e201a73dcaba0a970d-pi"
    };

    private RecyclerView rv;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private int selectUrlNum = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        rv = (RecyclerView)findViewById(R.id.rv);
        rv.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        rv.setLayoutManager(mLayoutManager);

        adapter = new CardAdapter(URLS, this);
        rv.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);
    }

    public void setSelectUrlNum(int selectUrlNum)
    {
        this.selectUrlNum = selectUrlNum;
    }

    @Override
    public void onClick(View v)
    {
        if(v.getId() == R.id.fab)
            startActivity(new Intent(this, B.class).putExtra("URL", URLS[selectUrlNum]));
    }
}
