package by.fortegroup.GolubovMax;

import android.app.Activity;
import android.util.DisplayMetrics;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Maksim on 06.04.2016.
 */
public class Utils
{
    public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
                int count=is.read(bytes, 0, buffer_size);
                if(count==-1)
                    break;
                os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }

    public static int getDisplayWidth(Activity activity)
    {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return  metrics.widthPixels;
    }

    public static int getHeightByAspectRatio(Activity activity, int w, int h)
    {
        return (getDisplayWidth(activity) / w)*h;
    }

}
