package by.fortegroup.GolubovMax;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Debug;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import java.net.URL;

/**
 * Created by Maksim on 06.04.2016.
 */
public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> implements CompoundButton.OnCheckedChangeListener
{
    private String[] imageUrls;
    private Activity activity;

    private int ivH = 0;

    private CompoundButton lastRb = null;
    private ImageLoader imageLoader;

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
    {
        if(isChecked) {
            buttonView.setChecked(isChecked);
            if(lastRb != null)
                lastRb.setChecked(false);
            lastRb = buttonView;

            ((A)activity).setSelectUrlNum((int) buttonView.getTag());
        }
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView iv;
        public RadioButton rb;
        public ViewHolder(View v)
        {
            super(v);
            iv = (ImageView)v.findViewById(R.id.iv);
            rb = (RadioButton)v.findViewById(R.id.rb);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CardAdapter(String[] myDataset, Activity activity) {
        this.activity = activity;
        this.imageUrls = myDataset;
        this.imageLoader = new ImageLoader(activity);
        this.ivH = Utils.getHeightByAspectRatio(activity, 16, 9);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        holder.rb.setTag(position);
        holder.rb.setOnCheckedChangeListener(this);
        holder.iv.getLayoutParams().height = ivH;

        imageLoader.DisplayImage(imageUrls[position], holder.iv);

        if(position == 0) {
            holder.rb.setChecked(true);
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount()
    {
        return imageUrls.length;
    }
}