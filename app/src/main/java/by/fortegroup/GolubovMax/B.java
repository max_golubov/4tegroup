package by.fortegroup.GolubovMax;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by Maksim on 06.04.2016.
 */
public class B extends AppCompatActivity
{
    private ImageView iv;
    private ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        iv = (ImageView)findViewById(R.id.iv);
        imageLoader = new ImageLoader(this);

        String url = getIntent().getStringExtra("URL");

        if(url != "")
        {
            imageLoader.DisplayImage(url, iv);
        }
    }
}
